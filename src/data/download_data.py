# -*- coding: utf-8 -*-
# download_data.py
from ucimlrepo import fetch_ucirepo
from pathlib import Path
import logging


def download_data(output_filepath, dataset_id=891):
    """Download UCI Machine Learning dataset and save it to a CSV file

    Args:
        output_filepath (str): Path to the output CSV file.
        dataset_id (int, optional): ID of the UCI Machine Learning dataset.
        Defaults to 891.
    """
    uci_dataset = fetch_ucirepo(id=dataset_id)

    uci_dataset.data.original.to_csv(output_filepath +
                                     '/' +
                                     uci_dataset.metadata.
                                     name.replace(" ", "_") +
                                     '.csv',
                                     index=False)

    logger = logging.getLogger(__name__)
    logger.info('downloading data')


if __name__ == '__main__':

    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    project_dir = Path(__file__).resolve().parents[2]

    download_data(output_filepath=str(project_dir / 'data/raw/'))
