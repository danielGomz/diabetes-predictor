Diabetes Predictor
==============================

## Overview

This project, named diabetes-predictor, uses the CDC Diabetes Health Indicators dataset to build a predictive model for classifying individuals into categories: diabetes, pre-diabetes, or healthy.

## Dataset

    Source: CDC Diabetes Health Indicators 
    Features: 35, including demographics, lab results, and survey responses
    Target Variable: Diabetes status, classification.